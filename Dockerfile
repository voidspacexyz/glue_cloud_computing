FROM python:3.6.6-alpine3.8
MAINTAINER VoidSpaceXYZ (null@voidspace.xyz)
RUN pip install flask youtube-dl
COPY flask-webservice.py /tmp/
EXPOSE 80
CMD python /tmp/flask-webservice.py --port 80 --host=0.0.0.0